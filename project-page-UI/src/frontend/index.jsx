import {
  Box,
  Button,
  ButtonGroup,
  Checkbox,
  DatePicker,
  Form,
  FormFooter,
  FormHeader,
  FormSection,
  HelperMessage,
  Label,
  RadioGroup,
  Range,
  RequiredAsterisk,
  Select,
  Stack,
  TextArea,
  Textfield,
  Toggle,
  useForm
} from "@forge/react";

export const FormAllFieldsExample = () => {
  const { getFieldId, register, handleSubmit } = useForm();

  const onSubmit = (data) => {
    console.log(data);
  };

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <FormHeader title="Form header">
        <Text>
          Required fields are marked with an asterisk. <RequiredAsterisk />
        </Text>
      </FormHeader>

      <FormSection>
        <Stack space="space.200">
          <Box>
            <Label labelFor={getFieldId("textfield")}>
              Textfield <RequiredAsterisk />
            </Label>
            <Textfield
              {...register("textfield", {
                required: true,
                maxLength: 5,
              })}
            />
            <HelperMessage>Helper message.</HelperMessage>
          </Box>

          <Box>
            <Label labelFor={getFieldId("textarea")}>Text area</Label>
            <TextArea placeholder="Long form text" {...register("textarea")} />
          </Box>

          <Box>
            <Label labelFor={getFieldId("datepicker")}>Date picker</Label>
            <DatePicker {...register("datepicker")} />
          </Box>

          <Box>
            <Label labelFor={getFieldId("select")}>Select</Label>
            <Select
              options={[
                { label: "Apple", value: "apple" },
                { label: "Banana", value: "banana" },
              ]}
              {...register("select")}
            />
          </Box>

          <Box>
            <Label labelFor={getFieldId("range")}>Range</Label>
            <Range {...register("range")} />
          </Box>

          <Box>
            <Label labelFor={getFieldId("checkbox")}>Checkbox</Label>
            <Checkbox label="Label" {...register("checkbox.A")} />
            <Checkbox label="Label" {...register("checkbox.B")} />
            <Checkbox label="Label" {...register("checkbox.C")} />
          </Box>

          <Box>
            <Label labelFor={getFieldId("radio")}>Radio group</Label>
            <RadioGroup
              options={[
                { name: "radio", value: "A", label: "Label" },
                { name: "radio", value: "B", label: "Label" },
                { name: "radio", value: "C", label: "Label" },
              ]}
              {...register("radio")}
            />
          </Box>

          <Box>
            <Label labelFor="toggle">Toggle label</Label>
            <Toggle {...register("toggle")} />
          </Box>
        </Stack>
      </FormSection>

      <FormFooter>
        <ButtonGroup>
          <Button appearance="subtle">Cancel</Button>
          <Button type="submit" appearance="primary">
            Submit
          </Button>
        </ButtonGroup>
      </FormFooter>
    </Form>
  );
};
